// TOFFI ?? The One File Forth Interpreter !! ;)
#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <glib-2.0/glib.h>
#include <SDL2/SDL.h>


////
//// Output macros: allow for diverting to other places in other code
//// by just replacing the macro with real code or whatever works 4u.
////
#define _vmout(...)       fprintf(vm->sout, __VA_ARGS__)
#define _vmerr(...)       fprintf(vm->serr, __VA_ARGS__)
#define _memlog(...)      fprintf(stdout, __VA_ARGS__)

#define PROMPT       ""
#define STACK_EMPTY  "<empty>"

#define POP1(VM, X)  STKCELL *X = stack_pop(VM);
#define PUSH_INT(N) stack_push(vm, eInt, (void*)&N);
#define PUSH_DOUBLE(D) stack_push(vm, eDouble, (void*)&D);
    


// types of stack cells
enum stkType { eInt, eUInt, eDouble, ePtr, eNull, eVar, eString };

// any operator that takes 2 off, puts 1 back
// todo: c strings in parallel to make printing easier!
enum binOp   { eAdd, eSub, eMul, eDiv, eMod, eRem };

// any operator that takes 1, puts 1 back
enum unaryOp { eNot };

// stack operators
enum stackOp { eDup };

// vm error states
enum vmError
  {
    eOk, eStackUnderflow, ePushedNull,
    eAllocFailed, eNumConversion, eBinOpFail,
    eArgError, eSyntaxError, eVarError, eWordError
  };


//// A constant pointer to a space
const char* Space = " ";

//// Used to mark a 'variable' slot as being present but not yet
//// having any actual value. We use this, the address of a fixed
//// string as as good a value as any.
const char* EmptyVarSlot = "<null-var>";


//// A VM encapsulates all state for a single running
//// SCF session. This allows multiple vm-s in multiple
//// threads doing multiple things at once.
//// NOTE: MAKE SURE YOU USE THREAD SAFE strtok_r() !!
//// NOTE: AND ANY OTHER NECESSARY C LIBRARY VARIANTS.
////
enum vmState { eInterpret, eCompile };

typedef struct _scf_vm
{
  void         *self;

  FILE         *sin;
  FILE         *sout;
  FILE         *serr;

  GList        *stack;
  gchar        *inputCopy;
  char         *saveptr; //for strtok_r()
  gchar        *errorMsg;

  enum vmError error;
  enum vmState state;
  ////
  //// run-time environments
  ////
  GTree        *words;
} SCFVM;


////
//// A single stack entry
////
typedef struct _stack_cell {
  enum stkType type;
  union {
    int          i;
    unsigned int ui;
    double       d;
    void         *ptr;
    GString      *string;
  } value;

} STKCELL;


////
//// A word definition
////
//// A word contains a pointer to its definition.
//// A definition is --just-- a list of other words that
//// existed at compile time. To execute a word all you
//// have to do is call the handler for it. Simples.
////
#define WTYPE_SYSTEM       0x01  // A core SCF word definition i.e. 'C' coded.
#define WTYPE_USER         0x02  // Was defined externally i.e. SCForth coded.
#define WTYPE_VAR          0x04  // Word unless VARIABLE.
#define WTYPE_COMPILE_ONLY 0x08  // Words that only operate in 'compile' state.


typedef void (*WordHandler)(SCFVM* vm);

typedef struct _word_def {

  unsigned int type;
  gchar        *name;

  union {
    WordHandler  C;
  } handler;

} WORDDEF;


////
//// Forward Declarations
////
void    print_cell(SCFVM *vm, STKCELL*);
void    *scf_calloc(SCFVM *vm, size_t, size_t);
void    scf_free();
STKCELL *stack_new_cell(SCFVM *vm, enum stkType);
GList   *stack_push(SCFVM *vm, enum stkType type, void* value);

void    *scf_calloc(SCFVM* vm, size_t len, size_t n);
void    scf_freecell(STKCELL *cell);

void    vm_fault(SCFVM *vm, enum vmError, const char* fmt, ...);

#define _alloc(VM,T) (T*)scf_calloc(VM, sizeof(T),1)
#define _free(P) scf_free((void*)P)



//// /////////////////////////////////////////////////////////////////
////
//// GTree Comparison Function for WORD access.
////
//// /////////////////////////////////////////////////////////////////
int word_compare(const void* p1, const void * p2) {
  return strcmp((char*)p1, (char*)p2);
}

static gboolean word_dumper(gpointer k, gpointer v, gpointer d) {
  WORDDEF *w = (WORDDEF*)v;
  printf("word: %s, sys? %d\n", w->name, w->type & WTYPE_SYSTEM);
  return 0;
}

static gboolean word_lister(gpointer k, gpointer v, gpointer d) {
  fprintf(stdout, "%s ", ((WORDDEF*)v)->name);
  return 0;
}


//// /////////////////////////////////////////////////////////////////
////
//// Static DICTIONARY of the built-in word definitions from launch.
////
//// /////////////////////////////////////////////////////////////////
#define SYSWORDENTRY(X,Y) {WTYPE_SYSTEM,X,&scf_##Y}
#define SYSWORD(X)        static void scf_##X(SCFVM*)
#define SYSDEF(X)         static void scf_##X(SCFVM* vm)


////
//// "Standard" FORTH Words
////
SYSWORD(bye);
SYSWORD(help);
SYSWORD(words);
SYSWORD(define_word);
SYSWORD(define_constant);
SYSWORD(not_word);
SYSWORD(cr);
SYSWORD(lf);
SYSWORD(print_stack);
SYSWORD(stack_pop);
SYSWORD(op_plus);
SYSWORD(op_minus);
SYSWORD(op_multiply);
SYSWORD(op_int_divide);
////
//// "SDL2" Extensions because I can.
////
//// "g_FOO" => "graphics_FOO" because typeinf sdl2 is (a) boring and
//// (b) up for a clash at some point!
////
SYSWORD(g_init);
SYSWORD(g_quit);
SYSWORD(g_revision);
SYSWORD(g_version);
SYSWORD(g_window_create);
SYSWORD(g_window_destroy);
////
//// Default system words... these are dynamically loaded into a GTree
//// instance for each VM that starts up. That's kind of wasteful and
//// implies that in the future...
////
//// @todo: Have two definition trees, one common to all VM-s and one
//// for each VM that defines words as it goes... there is the
//// *CONCEPT* here of a *GLOBAL* or *LOCAL* word definition... has
//// that been covered in the the annals of Forth... probably.
////
static WORDDEF ScForthWords[] = {
  SYSWORDENTRY("words",    words),
  SYSWORDENTRY("-word",    not_word),
  SYSWORDENTRY(":",        define_word),
  SYSWORDENTRY("constant", define_constant),
  SYSWORDENTRY("bye",      bye),
  SYSWORDENTRY("help",     help),
  SYSWORDENTRY("cr",       cr),
  SYSWORDENTRY("lf",       lf),
  SYSWORDENTRY(".s",       print_stack),
  SYSWORDENTRY(".",        stack_pop),
  SYSWORDENTRY("+",        op_plus),
  SYSWORDENTRY("-",        op_minus),
  SYSWORDENTRY("*",        op_multiply),
  SYSWORDENTRY("/",        op_int_divide),
  ////
  //// SDL2... the pattern <foo>+ and <foo>- mean that
  //// a <foo> is created(+) or destroyed(-)... I hope
  //// it feels intuitive!
  ////
  SYSWORDENTRY("g/init",     g_init),
  SYSWORDENTRY("g/quit",     g_quit),
  SYSWORDENTRY("g/version",  g_version),
  SYSWORDENTRY("g/revision", g_revision),
  SYSWORDENTRY("g/window+",  g_window_create),
  SYSWORDENTRY("g/window-",  g_window_destroy)
};


//// /////////////////////////////////////////////////////////////////
////
//// VIRTUAL MACHINE OPERATIONS
////
//// /////////////////////////////////////////////////////////////////

SCFVM *vm_open(void)
{
  SCFVM *vm = (SCFVM*)calloc(sizeof(SCFVM), 1);

  if (vm) {

    vm->self    = vm; // might be handy?
    vm->stack   = NULL;
    vm->saveptr = NULL;
    vm->error   = eOk;
    vm->state   = eInterpret;
    ////
    //// Default streams for world interaction.
    ////
    vm->sin  = stdin;
    vm->sout = stdout;
    vm->serr = stderr;
    ////
    //// Install default word definitions.
    ////
    vm->words = g_tree_new(&word_compare);
    int nWords = sizeof(ScForthWords) / sizeof(WORDDEF);

    for(int i=0; i < nWords; i++ ) {
      g_tree_insert(
	  vm->words,
	  (gpointer)ScForthWords[i].name,
	  (gpointer)&ScForthWords[i]);
    }
    //g_tree_foreach(vm->words, word_dumper, NULL); // could pass vm!
  }
  return vm;
}

void vm_close(SCFVM *vm)
{
  if (vm) {
    if (vm->stack) {
      GList *cell = vm->stack;
      while(cell) {
	scf_freecell((STKCELL*)cell->data);
	cell = cell->next;
      }
      _memlog("released vm stack\n");
    }

    //// release word definitions
    ////
    //// TODO: traverse and destory all WORDDEF name strings
    ////       OR use g_tree_new_full() with functions but I
    ////       dont want to learn that just now!
    ////
    printf("TODO: release all memory\n");
    g_tree_unref(vm->words);

    free(vm);
  }
}

void vm_fault(SCFVM *vm, enum vmError error, const char* fmt, ...)
{
  assert(vm);
  char buf[1024];
  va_list argp;

  vm->error = error;

  if (vm->errorMsg) {
    g_free(vm->errorMsg);
    vm->errorMsg = NULL;
  }

  va_start(argp, fmt);

  if (vsnprintf(&buf[0], sizeof(buf), fmt, argp) > 0) {
    vm->errorMsg = g_strdup(buf);
  }
  va_end(argp);
}

void vm_clear_fault(SCFVM *vm)
{
  assert(vm);
  vm->error = eOk;
  if (vm->errorMsg) {
    g_free(vm->errorMsg);
    vm->errorMsg = NULL;
  }
}


/**
 * VM: WORD LOOKUP.
 *
 * Given a string it returns NULL or the WORDDEF record that contains
 * the handler information for the word.
 *
 * \param  SCFVM* vm   -- the VM to search
 * \param  char*  word -- the candidate defined word
 *
 * \return WORDDEF* or NULL
 */
WORDDEF *vm_word(SCFVM *vm, char* word)
{
  WORDDEF *wordDef = NULL;
  assert(vm);

  if (word) {
    wordDef = g_tree_lookup(vm->words, word);
  }
  return wordDef;
}


static char* vm_next_token(SCFVM *vm)
{
  assert(vm);
  return strtok_r(NULL, Space, &vm->saveptr);
}

//// /////////////////////////////////////////////////////////////////
////
//// STACK OPERATIONS
////
//// /////////////////////////////////////////////////////////////////

STKCELL *stack_new_cell(SCFVM *vm, enum stkType type)
{
  assert(vm);
  STKCELL *cell = _alloc(vm, STKCELL);
  if (cell) {
    cell->type = type;
  }
  else {
    vm_fault(vm, eAllocFailed, "stack_new_cell");
  }
  return cell;
}

GList *stack_push(SCFVM *vm, enum stkType type, void* value)
{
  assert(vm);
  STKCELL *cell = stack_new_cell(vm, type);
  if (cell) {
    switch(type)
      {
      case eInt:    cell->value.i      = *((int*)value);     break;
      case eDouble: cell->value.d      = *((double*)value);  break;
      case ePtr:    cell->value.ptr    = value;
      case eString: cell->value.string = g_string_new(value);

      default:
	fprintf(stderr, "push type fail: %d\n", type);
	exit(255);
      }
    vm->stack = g_list_prepend(vm->stack, cell);
  }
  return vm->stack;
}


STKCELL *stack_pop(SCFVM *vm)
{
  assert(vm);
  STKCELL *cell = NULL;
  if (vm->stack) {
    cell = vm->stack->data;
    vm->stack = g_list_next(vm->stack);
  }
  else {
    vm_fault(vm, eStackUnderflow, "stack_pop");
  }
  return cell;
}


//// /////////////////////////////////////////////////////////////////
////
//// STACK MEMORY MANAGEMENT
////
//// /////////////////////////////////////////////////////////////////

void *scf_calloc(SCFVM *vm, size_t len, size_t n)
{
  assert(vm);
  void* data = calloc(len, n);

  if (!data) {
    vm_fault(vm, eAllocFailed,
	     "Failed to scf_calloc: %li bytes x %li\n", len, n);
  }
  _memlog("scf_calloc: %p, %li x %li\n", data, len, n);
  return data;
}


void scf_free(void *p)
{
  if (p) free(p);
  _memlog("scf:free %p\n", p);
}


void scf_freecell(STKCELL *c)
{
  if (c) {
    if (ePtr == c->type && c->value.ptr) {
      _memlog("scl_freecell: released prt: %p\n", c->value.ptr);
      free(c->value.ptr);
    }
    else if (eString == c->type && c->value.string) {
      _memlog("scl_freecell: released string: %p\n", c->value.string);
      g_string_free(c->value.string, 1);
    }
    free(c);
    _memlog("scl_freecell: %p\n", c);
  }
}


//// /////////////////////////////////////////////////////////////////
////
//// TOKEN PROCESSING / TYPE DETECTION
////
//// /////////////////////////////////////////////////////////////////

int is_integer(SCFVM *vm, const char *s, int *value)
{
  assert(vm);
  assert(s);

  for(int i=0; i<strlen(s); i++) {
    if (0==i && '-' == s[i]) {
      continue;
    }
    if (!isdigit(s[i])) {
      return 0;
    }
  }

  if (value) {
    *value = atoi(s);
  }

  return 1;
}


int is_double(SCFVM *vm, const char *s, double *value) {
  assert(vm);
  char* endptr = NULL;
  double number = strtod(s, &endptr);

  if ((errno == ERANGE &&
       (number == DBL_MAX || number == DBL_MIN))
      || (errno != 0 && number == 0)) {
    vm_fault(vm, eNumConversion, "strtod barfed on %s", s);
    return 0;
  }
  if (endptr == s) {
    vm_fault(vm, eNumConversion, "No digits were found in %s\n", s);
    return 0;
  }
  if (value) *value = number;
  return 1;
}


//// /////////////////////////////////////////////////////////////////
////
//// PRINTABLES
////
//// /////////////////////////////////////////////////////////////////

const char* scf_binop2str(enum binOp op) {
  switch(op) {
  case eAdd: return "+"; break;
  case eSub: return "-"; break;
  case eMul: return "*"; break;
  case eDiv: return "/"; break;
  case eMod: return "\%"; break;
  case eRem: return "rem"; break;
  default:
    return "binOp ?";
  }
}


//// /////////////////////////////////////////////////////////////////
////
//// BINARY OPERATORS
////
//// /////////////////////////////////////////////////////////////////

static void scf_binop(SCFVM *vm, enum binOp op)
{
  assert(vm);

  STKCELL *n1 = stack_pop(vm);
  STKCELL *n2 = stack_pop(vm);

  if (n1 && n2) {
    int v1 = n1->value.i;
    int v2 = n2->value.i;
    int result = 0;
    _vmout("scf_binop: %d %s %d\n", v1, scf_binop2str(op), v2);
    switch(op) {
    case eAdd: result = v1 + v2; break;
    case eSub: result = v1 - v2; break;
    case eMul: result = v1 * v2; break;
    case eDiv: result = v1 / v2; break;
    case eMod: result = v1 % v2; break;
    case eRem: result = (v1 / v2) % v2; break;
    default:
      vm_fault(vm, eBinOpFail, "bin_binop: %d\n", op);
      result = -1;
      break;
    }
    // reduce thrash: recycle n1 or n2 here? BY NOT
    // popping the second operand but operating on
    // in in-situ as the result destination ?!?!?!
    stack_push(vm, eInt, (void*)&result);
    scf_freecell(n1);
    scf_freecell(n2);
  }
  else {
    vm_fault(vm,
      eBinOpFail,
      "empty stack for: %s in vm @ %p",
      scf_binop2str(op), vm);
  }
}

//// NOT assert()-ing because bin_ does it...
SYSDEF(op_plus)       { scf_binop(vm, eAdd); }
SYSDEF(op_minus)      { scf_binop(vm, eSub); }
SYSDEF(op_multiply)   { scf_binop(vm, eMul); }
SYSDEF(op_int_divide) { scf_binop(vm, eDiv); }


static void scf_help(SCFVM *vm) {
  assert(vm);
  _vmout(
      "\n\nScForth wants to help. (vm:%p) Built: %s %s\n\n"
      "You should probably >RTFM< at this point but if you want to see\n"
      "what WORDs are currently defined use:\n\n"
      "\t\twords<RETURN>\n\n"
      "\tAnd remember, \"It's Forth Jim, but not as we know it...\"\n"
      "\n\n",
      vm,
      __DATE__, __TIME__);
}


static void print_cell_typed(SCFVM *vm, STKCELL *c) {
  assert(vm);
  if (c) {
    switch(c->type) {
    case eInt:
      _vmout("<int/%i> ", c->value.i);
      break;
      //case eUInt:
      //  _vmout("<uint/%ui>", c->value.ui);
      //  break;
    case eDouble:
      _vmout("<double/%f>", c->value.d);
      break;
    case ePtr:
      _vmout("<ptr/%p>", c->value.ptr);
      break;
    case eString:
      _vmout("<string/%s>", c->value.string->str);
      break;
    case eNull:
      _vmout("<ptr/null>");
      break;
    default:
      _vmout("<type-error>");
      break;
    }
  }
  else {
    vm_fault(vm, eArgError, "print_cell: null");
  }
}

void print_cell(SCFVM *vm, STKCELL *c) {
  assert(vm);
  if (c) {
    switch(c->type) {
    case eInt:
      _vmout("%i", c->value.i);
      break;
      //case eUInt:
      //  _vmout("<uint/%ui>", c->value.ui);
      //  break;
    case eDouble:
      _vmout("%f", c->value.d);
      break;
    case ePtr:
      _vmout("%p", c->value.ptr);
      break;
    case eString:
      _vmout("%s", c->value.string->str);
      break;
    case eNull:
      _vmout("null");
      break;
    default:
      _vmout("<type-error>");
      break;
    }
  }
  else {
    vm_fault(vm, eArgError, "<null-cell>");
  }
}


//// /////////////////////////////////////////////////////////////////
////
//// SC FORTH WORD IMPLEMENTATION
////
//// /////////////////////////////////////////////////////////////////

static void scf_bye(SCFVM *vm)
{
  assert(vm);
  vm_close(vm);
  exit(0);
}

//// CR and LF
static void scf_cr(SCFVM *vm) { assert(vm); fputc(13, stdout); }
static void scf_lf(SCFVM *vm) { assert(vm); fputc(10, stdout); }

//// ...what we do when a word is not defined...
static void scf_not_word(SCFVM *vm)
{
  assert(vm);
  vm_fault(vm, eSyntaxError, "Undefined word");
}


static void scf_stack_pop(SCFVM *vm)
{
  assert(vm);
  POP1(vm, cell);

  if (cell) {
    print_cell(vm, cell);
  }
  else {
    fprintf(stdout, STACK_EMPTY);
  }
}


static void scf_print_stack(SCFVM *vm)
{
  assert(vm);

  if (vm->stack) {

    GList *cell = g_list_last(vm->stack);

    if (cell) {

      fprintf(vm->sout, "<%u> ", g_list_length(cell));

      for(int i = 1; cell; i++, cell = cell->prev) {
	print_cell(vm, (STKCELL*)cell->data);
	_vmout(" ");
      }
    }
  }
  else {
    _vmout("<empty>\n");
  }
}


static void scf_words(SCFVM *vm) {
  assert(vm);
  if (vm->words) {
    g_tree_foreach(vm->words, word_lister, NULL);
  }
  _vmout("%s\n", PROMPT);
}


static void scf_execute_word(SCFVM *vm, WORDDEF *word)
{
  assert(vm);
  assert(word);
  ////
  //// If word is SYSWORD we call the handler otherwise
  //// we have to call each of the words that make up the
  //// definition instead until done.
  ////
  if (word->type & WTYPE_SYSTEM) {
    if (word->handler.C) {
      word->handler.C(vm);
    }
    else {
      fprintf(stderr,
	      "FATAL ERROR: corrupted sysword handler for %s",
	      word->name);
      exit(255);
    }
  }
  else if (word->type & WTYPE_VAR) {
    ////
    //// Load the value and push it onto stack
    ////
  }
  else {
    printf("USER WORD! %s ", word->name);
  }
}


static void scf_var_fetch(SCFVM *vm, char *s) {
  assert(vm);
  /*
    if (s) {
    char *identifier = strtok_r(NULL, Space, &vm->saveptr);
    if (identifier) {
    VARDEF *var = NULL; //g_hash_table_lookup(vm->vars, identifier);
    if (var) {
    //// got the variable, put its value on the stack
    _vmout("FETCH FOUND %s\n", identifier);
    }
    else {
    //// no variable exists
    vm_fault(vm, eVarError, "Unregistered variable: %s", identifier);
    }
    }
    }
  */
}


static void scf_new_var(SCFVM *vm, char *s) {
  //// variable NAME
  assert(vm);
  /*
    if (s) {
    char *identifier = strtok_r(NULL, Space, &vm->saveptr);
    if (identifier) {
    _vmout("VAR NAME: %s\n", identifier);
    //// if it already exists just do nothing
    if (!g_hash_table_contains(vm->vars, (gconstpointer)identifier)) {
    _vmout("new variable %s\n", identifier);

    if (!g_hash_table_insert(
    vm->vars,
    g_strdup(identifier),
    (gpointer)NULL))
    {
    vm_fault(
    vm, eAllocFailed,
    "failed to insert slot holder for %s\n",
    identifier);
    }
    }
    }
    }
  */
}


/**
 * CONSTANT. Defines an unchanging value.
 */
static void scf_define_constant(SCFVM *vm)
{
  assert(vm);
  
}


/**
 * The ":" Word. Defining a new word.
 *
 * This is where we define a new word. If the word already exists the
 * old definition is junked *after* a successful extraction and
 * compilation phase.
 */
static void scf_define_word(SCFVM *vm)
{
  assert(vm);

  char *identifier = vm_next_token(vm);

  if (identifier) {

    GList *def = NULL;
    char *wordStr = vm_next_token(vm);

    while(wordStr)
    {
      WORDDEF* word = g_tree_lookup(vm->words, wordStr);

      if (word) {
	//def = g_list_prepend(def, g_strdup(wordStr));
	//inst = strtok(NULL, Space);
      }
      else {
	vm_fault(vm, eSyntaxError, "%s is not defined.", wordStr);
	wordStr = NULL;
      }
    }
  }
}


//// /////////////////////////////////////////////////////////////////
////
//// WORDS: SDL2 INTEGRATION
////
//// /////////////////////////////////////////////////////////////////

SYSDEF(g_init) {
  ////
  //// -- n  ( status )
  ////
  assert(vm);
  int result = SDL_Init(SDL_INIT_EVERYTHING);
  PUSH_INT(result);
}

SYSDEF(g_version) {
  ////
  //// -- n n n  (major, minor, patched )
  ////
  assert(vm);
  SDL_version linked;
  SDL_GetVersion(&linked);
  PUSH_INT(linked.patch);
  PUSH_INT(linked.minor);
  PUSH_INT(linked.major);
}

SYSDEF(g_revision) {
  ////
  ////  -- c*  ( revision string )
  ////
  assert(vm);
  const char* info = SDL_GetRevision();
  stack_push(vm, eString, (void*)info);
}

SYSDEF(g_quit) {
  assert(vm);
  SDL_Quit();
}

SYSDEF(g_window_destroy) {
  assert(vm);
  POP1(vm, cell);
  if (cell) {
    SDL_DestroyWindow((SDL_Window*)cell->value.ptr);
  }
}

SYSDEF(g_window_create) {
  assert(vm);
  ////
  //// -- SDL_Window* or 0
  ////
  SDL_Window *window;
  SDL_Renderer *renderer;
  if (SDL_CreateWindowAndRenderer(
	  320, 240, SDL_WINDOW_RESIZABLE,
	  &window,
	  &renderer)) {
    SDL_LogError(
	SDL_LOG_CATEGORY_APPLICATION,
	"Couldn't create window and renderer: %s", SDL_GetError());
    stack_push(vm, ePtr, NULL);
  }
  else {
    stack_push(vm, ePtr, (void*)window);
  }
}


/**
 * Get more user input.
 *
 * This is the central part of the 'text interpreter' engine as described
 * by GForth documentation. Or at least my way.
 *
 * Having read the input it then duplicates the string data, saves it into
 * the VM state record and then calls strtok_r() to obtain a pointer to the
 * first token in the buffer.
 *
 * \param SCFVM* vm   -- VM context requiring the input
 *
 * \return char* -- the first token from the input
 */
static char* scforth_get_more_input(SCFVM *vm)
{
  assert(vm);

  char *input = (char*)calloc(1024, sizeof(char));

  if (input) {

    size_t len;

    if (vm->inputCopy) {
      g_free(vm->inputCopy);
      vm->inputCopy = NULL;
    }
    
    if (getline(&input, &len, vm->sin) > 0) {
      len = strlen(input);
      if (input[len-1] == '\n') {
	input[len-1] = 0;
      }
      vm->inputCopy = g_strdup(input);
      free(input);
    }
  }
  return strtok_r(vm->inputCopy, Space, &vm->saveptr);
}


/**
 * "Text Interpreter Engine".
 *
 * This does the core engine looping: read some user input then
 * causes each word to be processed. If there are errors we junk
 * the entire buffer and just go get some more.
 */
int scforth_main(SCFVM *vm)
{
  assert(vm);

  char input[1024];
  int n;
  double d;
  WORDDEF *word = NULL;
  char    *wordStr = NULL;

  for(;;) {
    switch(vm->state)
      {
      case eInterpret:
	{
	  wordStr = scforth_get_more_input(vm);
	  
	  while(wordStr) {

	    switch(vm->state)
	      {
	      case eInterpret:
		word = vm_word(vm, wordStr);
		//printf("\nWORDSTR: %s -> %p\n", wordStr, word);
	  
		if (word) {
		  scf_execute_word(vm, word);
		  ////
		  //// ok... this word MIGHT have changed the state so how
		  //// do we manage this? We should break out of the while
		  //// loop and switch to compile mode instead?
		  ////
		}
		else if (is_integer(vm, wordStr, &n)) {
		  ////
		  //// Integer first: any dots in a number will fail
		  //// the test, falling through to the double test.
		  ////
		  //printf("IS INT: %i\n", n);
		  PUSH_INT(n);
		}
		else if (is_double(vm, wordStr, &d)) {
		  ////
		  //// Not a word, not an integer... last chance!
		  ////
		  //printf("IS DOUBLE: %f\n", d);
		  PUSH_DOUBLE(d);
		}
		else {
		  // vm_fault?
		  fprintf(vm->serr, "WTF? Junked it. Try again...\n");
		}
		wordStr = strtok_r(NULL, Space, &vm->saveptr);
		break;
	      case eCompile:
		break;
	      }
	  }
	}
	break;

      case eCompile:
	break;
      
      default:
	fprintf(stderr, "Illegal VM state!\n");
	exit(255);
      }
  }
}


//// /////////////////////////////////////////////////////////////////
////
//// SC FORTH MAIN ENTRY POINT
////
//// /////////////////////////////////////////////////////////////////

int main(int argc, char* argv[])
{
  SCFVM *vm = vm_open();

  char buf[1024], c;

  if (vm) {

    _vmout("SCFORTH (yeah, mine) V.infinity.2\n");
    _vmout("'help' or 'words' is a good pwill get you started!\n\n");

    scforth_main(vm);
  }
  return 0;
}
